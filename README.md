- Choose a creative product and download their corresponding files before going forward:

| Creative Products | Realtek Audio Driver | Creative Softwares | GenKGA | Realtek Audio Console |
| --- | :---: | :---: | :---: | :---: |
| [THX TruStudio Pro™](https://sg.creative.com/corporate/pressroom?id=13184&id=13184) | [*THX_2020.7z ![Github all releases](https://img.shields.io/github/downloads/shibajee/realtek-uad-creative-legacy-mod/total.svg)](https://github.com/shibajee/realtek-uad-creative-legacy-mod/releases) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/Software/TAMB-GBS1D-4-LB.rar) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/GenKGA/GenKGA.rar) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/Software/RtkUWP.rar) |
| [Sound Blaster X-Fi MB2](https://www.creative.com/oem/products/software/x-fimb2.asp) | [*MB2_2020.7z ![Github all releases](https://img.shields.io/github/downloads/shibajee/realtek-uad-creative-legacy-mod/total.svg)](https://github.com/shibajee/realtek-uad-creative-legacy-mod/releases) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/Software/XFMB-AUS2D-1-LB.rar) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/GenKGA/GenKGA.rar) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/Software/RtkUWP.rar) |
| [Sound Blaster X-Fi MB3](https://www.creative.com/oem/products/software/x-fimb3.asp) | [*MB3_2020.7z ![Github all releases](https://img.shields.io/github/downloads/shibajee/realtek-uad-creative-legacy-mod/total.svg)](https://github.com/shibajee/realtek-uad-creative-legacy-mod/releases) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/Software/XMB3-OEM1D-4-11.rar) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/GenKGA/GenKGA.rar) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/Software/RtkUWP.rar) |
| [Sound Blaster X-Fi MB5](https://www.creative.com/oem/products/software/x-fimb5.asp) | [*MB5_2020.7z ![Github all releases](https://img.shields.io/github/downloads/shibajee/realtek-uad-creative-legacy-mod/total.svg)](https://github.com/shibajee/realtek-uad-creative-legacy-mod/releases) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/Software/XMB5-OEM1D-1-11.rar) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/GenKGA/GenKGA.rar) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/Software/RtkUWP.rar) | 
| [Sound Blaster Cinema](https://www.creative.com/oem/products/software/cinema.asp) | [*SBC_2020.7z ![Github all releases](https://img.shields.io/github/downloads/shibajee/realtek-uad-creative-legacy-mod/total.svg)](https://github.com/shibajee/realtek-uad-creative-legacy-mod/releases) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/Software/SBC-OEM1D-3-11.rar) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/GenKGA/GenKGA.rar) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/Software/RtkUWP.rar) |
| [Sound Blaster Cinema 2](https://www.creative.com/oem/products/software/cinema2.asp) | [*SBC2_2020.7z ![Github all releases](https://img.shields.io/github/downloads/shibajee/realtek-uad-creative-legacy-mod/total.svg)](https://github.com/shibajee/realtek-uad-creative-legacy-mod/releases) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/Software/SBC2-OEM1D-3-11.rar) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/GenKGA/GenKGA.rar) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/Software/RtkUWP.rar) |
| [Sound Blaster Cinema 3](https://www.creative.com/oem/products/software/cinema3.asp) | [*SBC3_2020.7z ![Github all releases](https://img.shields.io/github/downloads/shibajee/realtek-uad-creative-legacy-mod/total.svg)](https://github.com/shibajee/realtek-uad-creative-legacy-mod/releases) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/Software/SBC3-OEM1D-1-11.rar) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/GenKGA/GenKGA.rar) | [Download](https://github.com/shibajee/realtek-uad-creative-legacy-mod/raw/master/Archives/Software/RtkUWP.rar) |

**[mirror link](https://cloud.disroot.org/s/qJdSEiYnPMfA2KW)

## Instructions:
- Uninstall current realtek audio driver, if possible use [DDU](https://www.wagnardsoft.com/forums/viewtopic.php?f=5&t=2747) for clean driver uninstallation.

- Restart ur PC with Disable driver signature enforcement:

Go to Settings > Update & Security > Recovery > Advanced startup > click Restart now

Now go to Troubleshoot > Advanced options > Startup Settings > click Restart

After the restart u will get a Startup Settings page, press F7 to go into disable driver signature enforcement.

- Extract downloaded realtek driver (.7z archive) and double click Setup.exe

![alt text](https://i.postimg.cc/9QDrtMSq/Untitled-2.png)

Continue with the warning sign and click Install this driver software anyway. Do not restart, select I will restart my computer later.

- Enable Sideload apps:

Go to Settings > Update & Security > For developers > click Sideload apps and close.

- Now extract RtkUWP.rar and run INSTALL_RtkUWP.bat as administrator. This will install Realtek audio control panel. Installation is silent and will close automatically.

- Restart ur PC

- Extract the creative softwares (.rar archive) and double click setup.exe, complete the setup and select I will restart my computer later.

- Now extract GenKGA.rar and run GenKGA.bat as administrator, complete the activation.

(Some antivirus might detect GenKGA3.1.exe as false posivite like any other self made exe file, the source of GenKGA file can be found [HERE](https://pastebin.com/BHnvBYWD). Just disable ur antivirus/realtime protection or made the GenKGA3.1.exe as an exception list in antivirus.)

- Restart ur PC

- If everything is ok, creative software will launch automatically after the restart. Play a music file and try different presets or settings in the software to ensure that creative effects r working properly.

#### Uninstallation:

- First uninstall Realtek Audio Console from start menu, then uninstall creative software (sbc/thx/x-fi whatever) and finally uninstall 
realtek audio driver then restart (if possible use [DDU](https://www.wagnardsoft.com/forums/viewtopic.php?f=5&t=2747) for clean driver uninstallation)

#### Update or Upgrade:

- Uninstall current mod by above then install new driver.

-----


<p align="center"><img src=https://www.creative.com/oem/images/software/SBXpro_studio_animated.gif></p>

<p align="center"><img src=https://www.creative.com/oem/images/software/mb3_feature1.gif></p>

<p align="center"><img src=https://i.imgur.com/2MijIyD.png></p>

![alt text](https://www.creative.com/oem/images/software/md5_profile_red.png)

![alt text](https://www.creative.com/oem/images/software/04Music.png)


## FAQ:

- Which Windows version is compatible ?

Windows 10 64bit RS5 1809 to latest whatever version.

- Why no 32bit support ?

ƒuck 32bit.

- Which realtek audio chips are supported with this mod ?

Almost every audio chip they made.

- Any plan to add multiple APO in future ?

No, I like things simple and lightweight.

- Any update schedule for driver ?

Once in every month.
